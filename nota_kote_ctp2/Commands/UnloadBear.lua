function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload units.",
		parameterDefs = {
			{ 
				name = "theBear",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unloadPos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local theBear = parameter.theBear
	local unloadPos = parameter.unloadPos
	
	Spring.GiveOrderToUnit(theBear,CMD.UNLOAD_UNITS,{unloadPos.x+50,unloadPos.y,unloadPos.z,10},{})-- +50 jen kvuli ctp2
	
	return RUNNING
	
end


function Reset(self)
end
