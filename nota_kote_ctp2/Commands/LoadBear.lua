function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load units.",
		parameterDefs = {
			{ 
				name = "theBear",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitList",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

function Run(self, units, parameter)
	local theBear = parameter.theBear
	local unitList = parameter.unitList

	for i=1,#unitList do
		Spring.GiveOrderToUnit(theBear,CMD.LOAD_UNITS,{unitList[i]},{})
	end
	
	
	if self:UnitIdle(theBear) then
		return SUCCESS
	else
		return RUNNING
	end
	
end


function Reset(self)
end
