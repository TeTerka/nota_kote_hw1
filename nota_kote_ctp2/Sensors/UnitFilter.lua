local sensorInfo = {
	name = "UnitFilter",
	desc = "",
	author = "tk",
	date = "2018-04-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return filtered list of units
return function(allUnits,filterByName)	

		local count = #allUnits
		local filteredUnits = {}
		local n = 1
		for i=1, count do
			local def = Spring.GetUnitDefID(units[i])
			if UnitDefs[def].name == filterByName then
				filteredUnits[n] = units[i]
				n = n+1
			end
		end
		return filteredUnits
		
end