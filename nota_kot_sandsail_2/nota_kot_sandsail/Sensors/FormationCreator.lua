local sensorInfo = {
	name = "FormationCreator",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- @description return current wind statistics
return function()
	local count = #units
	local angleStep = 360/count
	local radius = 100
	local spots = {}
	for i=1, count do
		spots[i] = Vec3((radius * math.cos(math.rad((i-1)*angleStep))-radius),0,radius * math.sin(math.rad((i-1)*angleStep)))
	end
	return spots
end