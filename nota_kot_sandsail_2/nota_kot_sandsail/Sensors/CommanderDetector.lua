local sensorInfo = {
	name = "CommanderDetector",
	desc = "",
	author = "",
	date = "2017-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()	

		local count = #units
		local index = 0
		for i=1, count do
			local def = Spring.GetUnitDefID(units[i])
			if UnitDefs[def].humanName == "Battle Commander" then
				index = i
				break
			end
		end
		local cx, cy, cz = Spring.GetUnitPosition(units[index])
		return Vec3(cx+100,cy,cz)
		
end